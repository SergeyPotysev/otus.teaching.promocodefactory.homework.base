﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T> : IRepository<T> where T : BaseEntity
    {
        protected List<T> Data { get; set; }

        public InMemoryRepository(List<T> data)
        {
            Data = data;
        }
        
        
        public Task<List<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        
        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        
        public Task AddAsync(T entity)
        {
            Data.Add(entity);
            return Task.CompletedTask;
        }

        
        public Task UpdateAsync(T newItem)
        {
            var obsoleteItem = Data.Where(i=> i.Id == newItem.Id).First();
            var index = Data.IndexOf(obsoleteItem);
            Data[index] = newItem;
            
            return Task.CompletedTask;
        }

        
        public Task DeleteAsync(Guid id)
        {
            Data.RemoveAll(x => x.Id == id);
            return Task.CompletedTask;
        }
    }
}