﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники.
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IMapper _mapper;  

        public EmployeesController(IRepository<Employee> employeeRepository, IMapper mapper)
        {
            _employeeRepository = employeeRepository;
            _mapper = mapper;   
        }
        
        
        /// <summary>
        /// Добавить нового сотрудника.
        /// </summary>
        [HttpPost("add")]
        public async Task<ActionResult<EmployeeResponse>> AddEmployeeAsync(EmployeeRequest employeeModel)
        {
            try
            {
                var employee = _mapper.Map<Employee>(employeeModel);
                employee.Id = Guid.NewGuid();
                
                await _employeeRepository.AddAsync(employee);
                
                return Ok(employee);
            }
            catch
            {
                return BadRequest("Failed to create a new employee");
            }
        }
        
        
        /// <summary>
        /// Получить данные всех сотрудников.
        /// </summary>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = _mapper.Map<List<EmployeeShortResponse>>(employees);

            return employeesModelList;
        }
        

        /// <summary>
        /// Получить данные сотрудника по Id.
        /// </summary>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);
            if (employee == null)
                return NotFound();
            
            var employeeModel = _mapper.Map<EmployeeResponse>(employee);

            return employeeModel;
        }

        
        /// <summary>
        /// Изменить данные сотрудника.
        /// </summary>
        /// <param name="id">GUID сотрудника.</param>
        /// <param name="employeeModel">Изменяемая модель данных сотрудника.</param>
        /// <returns>Статус-код выполнения операции.</returns>
        [HttpPut("update/{id:guid}")]
        public async Task<IActionResult> UpdateEmployeeAsync(Guid id, EmployeeRequest employeeModel)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);
            if (employee == null)
                return NotFound();
            
            employee = _mapper.Map<Employee>(employeeModel);
            employee.Id = id;
            
            try
            {
                await _employeeRepository.UpdateAsync(employee);
                return Ok(employee);
            }
            catch
            {
                return BadRequest($"Failed to update employee with id:{id}");
            }
        }
        
        
        /// <summary>
        /// Удалить сотрудника.
        /// </summary>
        /// <param name="id">GUID сотрудника.</param>
        /// <returns>Статус-код выполнения операции.</returns>
        [HttpDelete("delete/{id:guid}")]
        public async Task<IActionResult> DeleteEmployeeAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);
            if (employee == null)
                return NotFound();
            
            try
            {
                await _employeeRepository.DeleteAsync(id);
                return Ok();
            }
            catch
            {
                return BadRequest($"Failed to delete employee with id:{id}");
            }
        }

    }
}