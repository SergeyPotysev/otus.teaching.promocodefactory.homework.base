using System.Linq;
using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Models;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Mappers
{
    public class AutoMapperEmployeeProfiles : Profile
    {
        public AutoMapperEmployeeProfiles()
        {
            CreateMap<RoleItemResponse, Role>().ReverseMap();
            
            CreateMap<EmployeeRequest, Employee>()
                .ForMember(d => d.Roles, o => o.MapFrom(
                    s => s.Roles.Select(c =>
                    new Role()
                    {
                        Id = c.Id,
                        Name = c.Name,
                        Description = c.Description
                    }).ToList()));          
                
            CreateMap<Employee, EmployeeResponse>()
                .ForMember(d => d.Roles, o => o.MapFrom(
                    s => s.Roles.Select(c =>
                    new RoleItemResponse()
                    {
                        Id = c.Id,
                        Name = c.Name,
                        Description = c.Description
                    }).ToList()));

            CreateMap<Employee, EmployeeShortResponse>();
        }
    }
}
