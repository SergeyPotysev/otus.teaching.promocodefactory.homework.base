﻿using System;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Models
{
    public class RoleItemResponse
    {
        public Guid Id { get; set; }
        
        public string Name { get; set; }
        
        public string Description { get; set; }
    }
}